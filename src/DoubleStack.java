import java.util.LinkedList;

// http://enos.itcollege.ee/~jpoial/algoritmid/magpraktuus.html
// http://algs4.cs.princeton.edu/13stacks/LinkedStack.java.html
// https://www2.hawaii.edu/~esb/2013fall.ics211/LinkedStack.java.html
// https://github.com/margusja/DoubleStack/blob/master/src/DoubleStack.java
// http://enos.itcollege.ee/~rrattas/Algoritmid_ja_andmestruktuurid_(I231)/Kodune3/src/Intstack.java

public class DoubleStack {

   public static void main (String[] argum) {
	   DoubleStack m = new DoubleStack();
		System.out.println(m);
		m.push(1);
		System.out.println(m);
		m.push(3);
		System.out.println(m);
		m.push(6);
		System.out.println(m);
		m.push(2);
		System.out.println(m);
		m.op("/");
		System.out.println(m);
		m.op("*");
		System.out.println(m);
		m.op("-");
		System.out.println(m);
		double result = m.pop();
		System.out.println(m);
		System.out.println(result);
		DoubleStack copy = m;
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		try {
			copy = (DoubleStack) m.clone();
		} catch (CloneNotSupportedException e) {
		}
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		m.push(6);
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		m.pop();
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		String prog = "2 3 + 4 * 10 /";
		if (argum.length > 0) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < argum.length; i++) {
				sb.append(argum[i]);
				sb.append(" ");
			}
			prog = sb.toString();
		}
		System.out.println(prog + "\n " 
                  + String.valueOf(interpret(prog)));
  
   }
   
   private LinkedList<Double> stack;
   
   DoubleStack() {
	 stack= new LinkedList <Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();
      clone.stack = (LinkedList<Double>) stack.clone();
      return clone;
   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }
   
   public void push (double a) {
	   stack.push(a);
   }

   public double pop() {
	   if (stack.size()==0){
		   throw new RuntimeException("Pole piisavalt elemente!");
	   }
	   return stack.pop();
      
   } // pop

   public void op (String s) {
	   
	   if (stack.size()<2){
		   throw new RuntimeException(s+"pole piisavalt elemente.");
	   }
	   double b = stack.pop();
	   double a = stack.pop();
	   
	   if (s.equals ("+")) 
	   {
		   //System.out.println(b +" "+ s +" "+ a);
		   stack.push (b + a);
	   }
	   else if (s.equals ("-")) 
	   {
		   //System.out.println(a +" "+ s +" "+ b);
		   stack.push (a - b);
	   }
	   else if (s.equals ("*")) 
	   {
		   //System.out.println(b +" "+ s +" "+ a);
		   stack.push (b * a);
	   }
	   else if (s.equals ("/")) 
	   {
		   //System.out.println(a +" "+ s +" "+ b);
		   stack.push (a / b);
	   }
	   else
		   throw new RuntimeException("Selline tehe "+s+" ei ole lubatud!");
   }
  
   public double tos() {
	   if (stack.size()==0){
		   throw new RuntimeException("Pole piisavalt elemente!");
	   }
	   return stack.peek();
	   
   }

   @Override
   public boolean equals (Object o) {
	   DoubleStack ds = (DoubleStack) o;
	   
	   if (stack.size() != ds.stack.size())
		   return false;
	   
	   
	   int i = 0;
	   for (double c: stack)
	   {
		   //System.out.println(c +" "+ ds.m.get(i));
		   if (c != ds.stack.get(i))
			   return false;
		  
		   i++;
	   }
      return true;
		
	   
	   
   }

   @Override
   public String toString() {
	   StringBuffer sb = new StringBuffer();
	   for (int i = stack.size() - 1;i >= 0; i--) {
		   		sb.append(stack.get(i));
	   }
	   return sb.toString();
   }

   public static double interpret (String pol) {
		DoubleStack stack = new DoubleStack();

		// Split input string into array elements
		String[] elements = pol.trim().split("\\s+");

		int i = 0;
		int op = 0;
		
		// Enhanced for loop
		for (String element : elements) {
			try {
				stack.push(Double.valueOf(element));
				i++;
			} catch (NumberFormatException e) {
				if (!element.equals("+") && !element.equals("-")
						&& !element.equals("*") && !element.equals("/"))
					throw new RuntimeException(
							"Expression contains illegal symbols " + element+pol);
				else if (stack.stack.size() < 2)
					throw new RuntimeException("Expression is too short!"+pol);
				stack.op(element);
				op++;
			}
		}
		if (i - 1 != op)
			throw new RuntimeException("Stack is out of balance!"+pol);
		return stack.pop();
   }

}

